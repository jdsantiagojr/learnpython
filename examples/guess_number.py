"""
Guess My Number

Computer picks a random number between 1 and 100. The player tries to
guess it and the computer lets the player know if the guess is too
high, too low or right on the money.
"""
import random


print("""
    \tWelcome to Guess My Number!
    \n\nI'm thinking of a number between 1 and 100.
    \nTry to guess it in as few attempts as possible.\n
    """)

number = random.randint(1, 100)
guess = int(input("Take a guess: "))
tries = 1

# guessing loop
while guess != number:
    print("lower..") if guess > number else print("higher..")
    guess = int(input("Take another guess: "))
    tries += 1

print("\nYou guess it! The number was", number)
print("It only took you", tries, "tries!\n")

input("\n\nPress enter key to exit.")
