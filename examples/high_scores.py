#!/usr/bin/env python3
"""
High Scores

Demonstrate nested sequences
"""
scores = []

choice = None
while choice != '0':
    print("""
    High Scores 2.0

    0 - Quit
    1 - List Scores
    2 - Add Score
    """)
    choice = input("Choice: ")
    print()

    # exit
    if choice == '0':
        print("Good-bye.")

    # display hisgh score table
    elif choice == '1':
        print("High Scores\n")
        print("Name\tScore")
        for entry in scores:
            score, name = entry
            print(name, "\t", score)

    # add a score
    elif choice == '2':
        name = input("What is the player's name?: ")
        score = input("What score did the player get?: ")
        entry = (score, name)
        scores.append(entry)
        scores.sort(reverse=True)

        # keep only top 5 scores
        scores = scores[:5]

    # some unknown choice
    else:
        print("Sorry, but", choice, "isn't a valid choice.")

input("\n\nPress enter key to exit.")
