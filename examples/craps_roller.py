#!/usr/bin/env python3
"""
Craps Roller

Demonstrates simple random number generation.
"""
import random


def roll_dice():
    dice = [random.randint(1, 6) for _ in range(2)]
    dice.append(sum(dice))
    return dice


def main():
    print("You rolled a {0} and a {1} for a total of {2}.\n".format(*roll_dice()))


if __name__ == '__main__':
    main()
