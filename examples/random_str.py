#!/usr/bin/env python3
"""
Random String
"""
import random
import string

sysrandom = random.SystemRandom()
punctuation = "!#$@"

def randstr(length=16):
    return "".join([sysrandom.choice(string.ascii_letters + string.digits + punctuation) for _ in range(length)])

print(randstr())
