#!/usr/bin/env python3.5
"""
Greetings

Demonstrates the print function and type hinting features
available in Python 3.5.

https://docs.python.org/3.5/library/typing.html
"""


def greet(name: str) -> str:
    """
    Returns new greeting message string.
    """
    return "Hello, {}!".format(name.title())


def main():
    # get the user's name
    name = input("\n\x1b[036mWhat is your name?\x1b[0m ")

    # display greeting message
    print(greet(name))

    input("\n\nPress the enter key to exit.\n")

if __name__ == '__main__':
    main()
