#!/usr/bin/env python3
"""
Range Counter

Demonstrates the range function.
"""
print("Counting: ")
for i in range(10):
    print(i, end=" ")

print("\n\nCounting in fives:")
for i in range(0, 50, 5):
    print(i, end=" ")

print("\n\nCounting backwards:")
for i in range(10, 0, -1):
    print(i, end=" ")

input("\n\nPress enter key to exit.")
