#!/usr/bin/env python3
"""
Sequence Membership

Python’s membership operators test for membership in a sequence, such
as strings, lists, or tuples.
"""

database = [
    ("santana", "1234"),
    ("rodriguez", "4242"),
    ("cruz", "1111")]


def main():
    username = input("username: ")
    password = input("password: ")

    print("Access Granted" if (username, password) in database else "Access Denied")

    input("\n\nPress the enter key to exit.\n")

if __name__ == '__main__':
    main()
