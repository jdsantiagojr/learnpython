#!/usr/bin/env python3.5
"""
Binary Search

https://en.wikipedia.org/wiki/Binary_search_algorithm
"""
# create mock data
data = [(i + 1) for i in range(100)]


def binary_search(a: list, key: int, imin: int, imax: int) -> int:
    """
    Recursive binary search function.
    :param a:
    :param key:
    :param imin:
    :param imax:
    :return:
    """
    if imax < imin:
        return "KEY_NOT_FOUND"

    # calculate the midpoint to cut set in half
    midpoint = (imin + imax) // 2
    midvalue = a[midpoint]

    # determine which sub-array to search
    if midvalue > key:
        # search lower
        return binary_search(a, key, imin, midpoint - 1)
    if midvalue < key:
        # search upper
        return binary_search(a, key, midpoint + 1, imax)
    # key was found
    return midpoint

print("Created list of 100 integers.")
number = input("Enter number to search for: ")

result = binary_search(data, int(number), 0,  len(data) - 1)

print(result)
